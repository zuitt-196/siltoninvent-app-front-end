import React,{useState,useEffect} from 'react'
import { SpinnerImage } from '../../loader/Loader'
import "./productList.scss";
import {FaEdit, FaRegTrashAlt} from 'react-icons/fa';
import {HiOutlineEye} from 'react-icons/hi'
import {useSelector, useDispatch} from 'react-redux';

// INMPORT REACT-CONFIRM-ALERT
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css


// AIMPOPRT FROM filterSLice file made by redux
import {selectFilterProduct,FILTER_PRODUCT} from '../../../redux/features/product/filterSlice';
import { deleteProduct, getAllProducts } from '../../../redux/features/product/productSlice';


// import seacrch component 
import Search from "../../search/Search"

//PAGINATION SECTTION 
import ReactPaginate from "react-paginate";
import { Link } from 'react-router-dom';




const ProductList = ({allProduct, isLoading}) => {
    //define the selectFilterProduct function in variable
    const filterProducts = useSelector(selectFilterProduct);

    // console.log(allProduct)

    const dispatch = useDispatch();

    // define the searh statw with the initialization of emtpy string 
    const [search, setSearch] = useState("");

const shortenRtext = (text, n)  =>{
        if (text.length > n ) {
            const shortened = text.substring(0,n).concat("...")
            return shortened
        }
        return text
}

// define the delete product 
const deleteProductByid = async (id) =>{
    await dispatch(deleteProduct(id));
    // after delete the products has to get the products
    await dispatch(getAllProducts())
}

// FUNCRION CONFIRM DELETE  THIS FUNCTIO IS MADE UP THE PACKAGE CODE IN  NPM 
    const confirmmDelete = (id) => {
        confirmAlert({
            title: 'Delete Product',
            message: 'Are you sure want delete this product? ',
            buttons: [
              {
                label: 'Delete',
                onClick: () => deleteProductByid(id)
              },
              {
                label: 'Cancel',
                // onClick: () => alert('Click No')
              }
            ]
          });
    }


// based 

// BEGIN PAGINATION IMPORT  FROM NPM , THIS OLD VERSION CODE THAT GIVERN IN NPM PAGINNATION
const [currentItems, setCurrentItems] = useState([]);
const [pageCount, setPageCount] = useState(0);
const [itemOffset, setItemOffset] = useState(0);
const itemsPerPage = 5;

useEffect(() => {
  const endOffset = itemOffset + itemsPerPage;
  // console.log('currentItems:', filterProducts.slice(itemOffset, endOffset))

  setCurrentItems(filterProducts.slice(itemOffset, endOffset));
  setPageCount(Math.ceil(filterProducts.length / itemsPerPage));
}, [itemOffset, itemsPerPage, filterProducts]);


const handlePageClick = (event) => {
  // console.log('curr event:', event)
  // console.log('curr filterProducts:', filterProducts)
  // console.log('curr proc:', (event.selected * itemsPerPage) % filterProducts.length)
  const newOffset = (event.selected * itemsPerPage) % filterProducts.length;
  setItemOffset(newOffset);
};
// END PAGINATION





useEffect(() => {
        dispatch(FILTER_PRODUCT({allProduct, search}))
}, [dispatch,allProduct,search])


  return (
    <div className="product-list">
      <hr />
        <div className="table">
                <div className="--flex-between --flex-dir-column">
                        <span><h3>Inventory Items</h3></span>
                        <span>
                                <Search value={search} onChange={(e)=> setSearch(e.target.value)}/>
                        </span>
                </div>
     
            {isLoading && <SpinnerImage/>}
            
                <div className="table">
                    {/* if the loading is done */}
                        {!isLoading && allProduct === 0? (
                                <p>-- No product found, please add a product... </p>
                        ):(
                            <table>
                                    <thead>
                                            <tr>
                                                <th>s/n</th>
                                                <th>Name</th>
                                                <th>Category</th>
                                                <th>Price</th>
                                                <th>Quantity</th>
                                                <th>Value</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                        <tbody>
                                            {currentItems && currentItems.map((product, index) => {
                                                    const {_id, name ,category , price, quantity} = product;
                                                    return (
                                                        <tr key={_id}>
                                                            {/* {console.log(_id)} */}
                                                            <td>{index + 1}</td>
                                                            <td>{shortenRtext(name,16)}</td>
                                                            <td>{category}</td>
                                                            <td>{"$"}{price}</td>
                                                            <td>{quantity}</td>
                                                            <td>{"$"}{price * quantity}</td>
                                                            <td className='icons'>
                                                                     <span>
                                                                        <Link to={`/product-detail/${_id}`}>
                                                                                <HiOutlineEye size={25} color={"purple"}/>
                                                                        </Link>
                                                                    </span>
                                                                    <span>
                                                                        <Link to={`/product-update/${_id}`}>
                                                                                  <FaEdit size={25} color={"green"}/>
                                                                        </Link>
                                                                    </span>
                                                                    <span>
                                                                        <FaRegTrashAlt size={20} color={"red"} onClick={() =>confirmmDelete(_id)}/>
                                                                    </span>
                                                            </td>
                                                        </tr>      
                                                    )
                                            })}
                                            
                                        </tbody>
                            </table>
                        )}
                </div>
                <ReactPaginate
          breakLabel="..."
          nextLabel="Next"
          onPageChange={handlePageClick}
          pageRangeDisplayed={5}
          pageCount={pageCount}
          previousLabel="Prev"
          renderOnZeroPageCount={null}
          containerClassName="pagination"
          pageLinkClassName="page-num"
          previousLinkClassName="page-num"
          nextLinkClassName="page-num"
          activeLinkClassName="activePage"
        />
       </div>
    </div>
  )
}

export default ProductList
