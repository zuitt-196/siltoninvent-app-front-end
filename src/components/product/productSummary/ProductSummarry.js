import React,{useEffect} from 'react'
import "./ProductSummary.scss"
import {AiFillDollarCircle} from 'react-icons/ai';
import {BsCart4,BsCartX} from 'react-icons/bs';
import {BiCategoryAlt} from "react-icons/bi";
import InfoBox from '../../infoBox/InfoBox';
import { useDispatch, useSelector } from 'react-redux';   
import { CACL_STORE_VALUE, CACL_OUTOFSTOCK,  CACL_CATEGORAY, selecCategory,selectOutOfStock, selectTotalStoreValue } from '../../../redux/features/product/productSlice';




//ICONS
const earningIcon = <AiFillDollarCircle size={40} color="#fff"/>
const productIcon = <BsCart4 size={40} color="#fff"/>
const categoryIcon = <BiCategoryAlt size={40} color="#fff"/>
const outOfStock = <BsCartX size={40} color="#fff"/>


// FORMAT AMOUNT 
export const formatNumbers = (x) => {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

};

const ProductSummarry = ({products}) => {
  const dispatch = useDispatch();
  const totalStoreValue = useSelector(selectTotalStoreValue);
  // define the outOfstock
  const outOfstackProduct = useSelector(selectOutOfStock);

// define the categoreis 
  const cateogry  = useSelector(selecCategory);
// defiene the 
  const totalProducts = products.length;

  useEffect(() => {
      dispatch(CACL_STORE_VALUE(products));
      dispatch(CACL_OUTOFSTOCK(products));
      dispatch(CACL_CATEGORAY(products));


  }, [dispatch, products])

  return (
    <div className='product-summary'>
        <h3>Inventory Stats</h3>
        <div className='info-summary'>
            <InfoBox icon={productIcon } title={"Total Products"} count={totalProducts } bgColor="card1"/>
            <InfoBox icon={ earningIcon  } title={"Total Store Value"} count={ `$ ${formatNumbers(totalStoreValue.toFixed())}`} bgColor="card2"/>
            <InfoBox icon={outOfStock  } title={"Out of Stock"} count={outOfstackProduct} bgColor="card3"/>
            <InfoBox icon={categoryIcon} title={"All Categories"} count={cateogry.length} bgColor="card4"/>
        </div>
    </div>
  )
}

export default ProductSummarry