import React, {useState}from 'react'
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { changePassword } from '../../services/authServices';
import Card from '../card/Card';
import './ChagesPassword.scss';


const initialState = {
    oldpassword: "",
    password: "",
    confirmPassword: "",

  }
  

const ChangesPassword = () => {
    
    const navigate = useNavigate()

    const [formData, setFormData] = useState(initialState);


    // distracture the formData 
    const {oldpassword , password, confirmPassword} = formData;



    
// define  functiion that changes values in input elemenent
const handleInputChange = (e) => {
    const {name , value} = e.target
    setFormData({...formData, [name]:value});
  
  }

// define chnages pass

  const changePass = async (e) => {
    e.preventDefault();
        if (password !== confirmPassword) {
            return toast.error("New Password did not match")
        }

        const formData = {
                oldpassword,
                 password

        }
        
       const data = await changePassword(formData);
        toast.success(data)
        navigate("/login")
  }




  return (
    <div className="change-password">
            <Card cardClass={"password-card"}>
                    <form  onSubmit={changePass } className="--form-control">
                    <input type="password" 
                    placeholder='Old Password'
                    required
                    name='oldpassword'
                    value={oldpassword}
                    autoComplete="on"
                    onChange={handleInputChange}    
                     />
                        <input type="password" 
                    placeholder='New Password'
                    required
                    name='password'
                    value={password}
                    autoComplete="on"
                    onChange={handleInputChange}    
                     />
                        <input type="password" 
                    placeholder='Confirm Password'
                    required
                    name='confirmPassword'
                    value={confirmPassword}
                    autoComplete="on"
                    onChange={handleInputChange}    
                     />

                     <button type= "submit" className="--btn --btn-primary">Change or Reset Password </button>
                    </form>
            </Card>
    </div>
  )
}

export default ChangesPassword