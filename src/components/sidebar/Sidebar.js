import React,{useState} from 'react'
import './Sibebar.scss'
import {BiMenuAltRight } from 'react-icons/bi';
import {RiProductHuntFill} from 'react-icons/ri'
import {useNavigate} from "react-router-dom"
  
  

import SidebarItem from './SidebarItem';
// IMPORT MENU FROM DATA FOLDER
import menu from '../../data/sidebar.js'

const Sidebar = ({children}) => {
  const [isOpen, seTisOpen] = useState(true);
  // console.log(isOpen); 
  const navigate = useNavigate();


// define to goHOme function to navgiate the home page 
 const goHome = () => {
  navigate("/")
  
 }


  const toggle = () => {
    seTisOpen(!isOpen)
  }
  
  return (
    <div className="layout">
        <div className="sidebar" style={{width: isOpen ? "230px": "60px"}}>
           <div className="top_section">
            <div className="logo" style={{display: isOpen ? "block": "none"}} >
                  <RiProductHuntFill size={35} style={{cursor: "pointer"}} onClick={goHome}/>
             </div>
             <div className="bars"  style={{marginLeft: isOpen ? "100px" : "0px"}}>
               <BiMenuAltRight size={35} onClick={toggle}/>
             </div>
           </div>

           {/* MENU SECTION */}
           {menu.map((item,index) => {
                return  <SidebarItem key={index} item={item} isOpen={isOpen} />
            })}
        </div>

        <main style={{paddingLeft: isOpen ?  "230px" : " 60px", transition: "all .5s"}}>
          {children}
        </main>
    </div>
  )
}

export default Sidebar
