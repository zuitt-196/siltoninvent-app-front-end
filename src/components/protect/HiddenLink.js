
import {useSelector} from "react-redux"

import {selectisLoggedIn} from "../../redux/features/auth/authSlice"


// define the show Login 
export const ShowOnlogin = ({children}) => {
     const isLoggedIn = useSelector(selectisLoggedIn);

     if (isLoggedIn) {
          return <div>{children}</div>
     }else{
        return null
     }
}

export const ShowOnlogout = ({children}) => {
    const isLoggedIn = useSelector(selectisLoggedIn);

    if (!isLoggedIn) {
         return  <div>{children}</div>
    }else{
       return null
    }
}