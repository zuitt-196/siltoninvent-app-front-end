import { createSlice } from '@reduxjs/toolkit'



 // define to  data store the local host web
const name = JSON.parse(localStorage.getItem('name'))

// initialize or default  property and value 
const initialState = {
    isLoggedIn: false,
    name:name ? name: "",
    user: {
      name: "",
      email: "",
      phone: "",
      bio: "",
      photo: ""
    },
    
    } 
  

// reducer and update the state with chnages the action 
const authSlice = createSlice({
  name:"name",
  initialState,
  reducers: {
    SET_LOGIN(state,action){
          state.isLoggedIn = action.payload
    },
    SET_NAME(state,action){
      // setItem to store a data
      localStorage.setItem("name", JSON.stringify(action.payload))
      state.name = action.payload
  },
  SET_USER(state,action){
    const profileUser = action.payload;
    state.user.name= profileUser.name;
    state.user.email= profileUser.email;
    state.user.phone= profileUser.phone;
    state.user.bio= profileUser.bio;
    state.user.photo= profileUser.photo;
    // console.log("statephoto:",state.user.photo);
},
 
  }
});

export const {SET_LOGIN, SET_NAME, SET_USER} = authSlice.actions


// export the some state that made of reducer that has manage the data and able to use it as  function some components anyware in applicaation
// it use for useSeletor
export const selectisLoggedIn = (state) => {
  // console.log(state.auth)
  return state.auth.isLoggedIn
   
}

export const selecteName = (state) => {
  return state.auth.name
}

export const selectisUser = (state) => {
// console.log("editUser:", state.auth.usep
  return state.auth.user
}
// or one line arrow function 
// export const selectisLoggedIn = (state) => state.auth.isLoggedIn


export default authSlice.reducer