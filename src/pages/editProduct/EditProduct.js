import React, {useEffect, useState}  from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate, useParams } from 'react-router-dom'
import Loader from '../../components/loader/Loader'
import ProductForm from '../../components/product/productForm/ProductForm'
import { getAllProducts, getProductByid, selectIsLoading, selectProduct, updateProductById } from '../../redux/features/product/productSlice'




const EditProduct = () => {

  const {id} = useParams()

      // defien the useDispath get action and changes the goes to redux
      const dispatch = useDispatch()
      // deifine naviagtion 
      const navigate = useNavigate()



     // define for isloading came from producSlice
      const isloading = useSelector(selectIsLoading);

      // define productEdit frunction to with the id of product 
      const productEdit = useSelector(selectProduct);

        // console.log("productiStrue:" , productEdit ? true : false);

      // use this to edit and save in database 
      const [product, setProduct] = useState(productEdit);
      const [productImage, setProductImage] = useState("");
      const [imagePreview, setImagePreview] = useState(null);
      const [desc, setDescription]  = useState(null);

// define  functiion that changes in input elemenent
const handleInputChange = (e) => {
  const {name , value} = e.target
  setProduct({...product, [name]:value});

}

// define the image change
const handleImageChange = (e) => {
  // to target the value which is file
  setProductImage(e.target.files[0]);

  // to set image static method creates a string containing a URL representing the object given in the parameter.
  // or them main point the url of file in local or static 
  setImagePreview(URL.createObjectURL(e.target.files[0]))
    }

  useEffect(() => {
      dispatch(getProductByid(id))
      

  }, [dispatch,id])
 
      useEffect(() => {
        setProduct(productEdit)
        // define the  imagePreview state 
        setImagePreview(productEdit && productEdit.image ? `${productEdit.image.filePath}` : null);

        setDescription(productEdit && productEdit.desc ? productEdit.desc : "");

      }, [productEdit])


      // define to save the product from editproduct function 
      const saveProduct = async (e) => {
        e.preventDefault();
        // create a form data with use of new FormData() constructor as to form an object able to 
        const formData = new FormData();
        formData.append("name",product?.name);

        // formData.append("sku",generateSKU(category)); remove this since SKU it can't change
        formData.append("category", product?.category);
        formData.append("quantity",product?.quantity);
        formData.append("price",product?.price);

        formData.append("desc",desc);
        // formData.append("image",productImage);
        if (productImage) {
          formData.append("image",productImage);     
        }
        
        // console.log(...formData);
      
        // pass dispatch or dispatch in redux center store data applicatio all 
        await dispatch(updateProductById({id, formData}));
        // after update the product data retrive all products
        await dispatch(getAllProducts())
        navigate("/dashboard")
}




  return (
    <div>
       {isloading && <Loader/>}
        <h3 className="--mt">Add New product</h3>
        <ProductForm
        product={product}
        productImage={productImage}
        imagePreview={imagePreview}
        description={desc}
        setDescription={setDescription}
        handleInputChange={handleInputChange}
        handleImageChange ={handleImageChange }
        saveProduct ={ saveProduct }
        />
    </div>
  )
}

export default EditProduct 