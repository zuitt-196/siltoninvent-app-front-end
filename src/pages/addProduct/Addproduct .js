import React,{useState} from 'react';
import ProductForm from '../../components/product/productForm/ProductForm';
import {useSelector} from "react-redux";
import { createProduct, selectIsLoading } from '../../redux/features/product/productSlice';
import { useDispatch } from 'react-redux';
import {  useNavigate } from 'react-router-dom';
import Loader from '../../components/loader/Loader';

const initialState ={
    name:"",
    category:"",
    quantity:"",
    price:"",
    }

const Addproduct  = () => {
    // defien the useDispath get action and changes the state
    const dispatch = useDispatch()

    // deifine naviagtion 
    const navigate = useNavigate()

    // define and initialize the defaultq product property
    const [product, setProducts] = useState(initialState);

    // define and initialize the default as empty string 
    const [productImage, setProductImage] = useState("");

    // defiene and inetialize the  default as null
    const [imagePreview, setImagePreview] = useState(null);

    // define aand initialize the default as emtpy sytring
    const [desc, setDescription]  = useState(null);

    // define for isloading came from producSlice
    const isloading = useSelector(selectIsLoading);

    // destruring the product
const {name ,category,quantity,price} = product

        
// define  functiion that changes in input elemenent
const handleInputChange = (e) => {
    const {name , value} = e.target
    setProducts({...product, [name]:value});

  }

  // define the image change
  const handleImageChange = (e) => {
    // to target the value which is file
    setProductImage(e.target.files[0]);

    // to set image static method creates a string containing a URL representing the object given in the parameter.
    // or them main point the url of file in local or static 
    setImagePreview(URL.createObjectURL(e.target.files[0]))
      }

// difien and genereate a SKU ans by own function without packages library in our aplication
// thw argumnet that we used is the category state of data
const generateSKU  = (category) => {
    const letter = category.slice(0,3).toUpperCase();
    const uniqueNumber = Date.now();
    const sku = letter + "-" + uniqueNumber;
    return sku
    
}

// save product inside the new construtor method new FormData()
const saveProduct = async (e) => {
        e.preventDefault();
        // create a form data with use of new FormData() constructor as to form an object able to 
        const formData = new FormData();
        formData.append("name",name);
        formData.append("sku",generateSKU(category));
        formData.append("category",category);
        formData.append("quantity",Number(quantity));
        formData.append("price",price);
        formData.append("desc",desc);
        formData.append("image",productImage);

        console.log(...formData);
       
        // pass dispatch or dispatch in redux center store data applicatio all 
        await dispatch(createProduct(formData))
        navigate("/dashboard")
}



  return (
    <div>
        {isloading && <Loader/>}
        <h3 className="--mt">Add New product</h3>
        <ProductForm 
        product={product}
        productImage={productImage}
        imagePreview={imagePreview}
        description={desc}
        setDescription={setDescription}
        handleInputChange={handleInputChange}
        handleImageChange ={handleImageChange }
        saveProduct ={ saveProduct }
        />
    </div>
  )
}

export default Addproduct 