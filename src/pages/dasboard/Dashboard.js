import {useEffect} from 'react'
import { useDispatch, useSelector } from 'react-redux';
import ProductList from '../../components/product/productList/ProductList';
import ProductSummarry from '../../components/product/productSummary/ProductSummarry';
import useRedirectLoogeOutUse from '../../customHook/useRedirectLoogeOutUse';
import { getAllProducts} from '../../redux/features/product/productSlice';
import { selectisLoggedIn} from "../../redux/features/auth/authSlice";


const Dashboard = () => {
  useRedirectLoogeOutUse("/login")

  const dispatch = useDispatch();

  const isLoggdIn = useSelector(selectisLoggedIn)
  // console.log('isLoggdIn:', isLoggdIn)
  // define the list of property came from redux state
  const  { allProduct, isError, isLoading ,message} = useSelector((state) => state.product);




  useEffect(() => {
    if(isLoggdIn === true){
      // console.log(123)
        // if  the user is logged in or true
      dispatch(getAllProducts())
    }
    // console.log(allProduct);
    if (isError) {
      console.log(message);
    }
  }, [isLoggdIn, isError, message,dispatch, getAllProducts]);

  return (
    <div>

    <ProductSummarry products={allProduct}/>
    <ProductList allProduct={allProduct} isLoading={isLoading} />
     </div>
  )
}

export default Dashboard