import React,{useState} from 'react'
import styles from "./auth.module.scss"
import {AiOutlineMail} from "react-icons/ai"
import Card from '../../components/card/Card'
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify' 
import { forgotpassword, validateEmail } from '../../services/authServices';



const ForgotPassword = () => {
  // define theb default state of email
  const [email, setEmail] = useState("");


  // difine the forgot function 
  const forgot = async (e) => {
    e.preventDefault();

    // validate the email if they are exist in feild input
    if (!email) {
        return toast.error("Pleae Enter an email");
    }

    // Please enter a valid email
    if(!validateEmail){
  return toast.error("Please Enter a valid email")
    }

    // define the email 
  const userData = {
    // email:email --> old version to take the data property
    email  // -->  if the property and value are the same, it  allowed to single property has no value
  }

  // define the email put the argument in call backfunction 
  await forgotpassword(userData)
  setEmail("")


  }
  return (

    <div className={`container ${styles.auth}`}>
        
    <Card>   
      <div className={styles.form}>
          <div className="--flex-center">
                  <AiOutlineMail size={35}/>
          </div>
          <h2>Forgot Password</h2>


          {/* FORM SECTION */}
          <form onSubmit={forgot}>
                <input type="email" 
                  placeholder='Email..'
                  required
                  name='email'
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
                 

                <button type='submit' className='--btn --btn-primary --btn-block'>Get Reset Email</button>
                <div className={styles.links}>
                          <p>
                            <Link to="/">-Home</Link>
                          </p>

                          <p>
                            <Link to="/login">-Login</Link>
                        </p> 
                          
                  </div>
                </form>
     </div>
        </Card>
    </div>
  )
}

export default ForgotPassword
