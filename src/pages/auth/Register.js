import React,{useState}from 'react'
import styles from "./auth.module.scss"
import {TiUserAddOutline} from "react-icons/ti"
import Card from '../../components/card/Card'
import { Link, useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify' 
import {validateEmail} from "../../services/authServices";
import {resgiterUser} from "../../services/authServices";


//IMPORT S DESPATCH FROM REAC-REDUX 
import {useDispatch} from "react-redux";

//IMPORT SLICE THAT CONTAIN OF REDUCER METHPO OR FUNCTION 
import {SET_LOGIN, SET_NAME } from '../../redux/features/auth/authSlice'
import Loader from '../../components/loader/Loader'




const initialState = {
  name: "",
  email: "",
  password: "",
  confirmpassword:"",
}

const Register = () => {
  // defien the dispatch method 
  const dispatch = useDispatch();

  // define the navigiation 
  const navigate = useNavigate();

  // define the loading 
  const [isLoading, setIsloading] = useState(false);
  // define and initial the  default data in register componenst
  const [formData, setFormData] = useState(initialState);

  // defracture the formData 
  const {name,email, password,   confirmpassword} = formData;

  // define the changes in input elemenent
const handleInputChange = (e) => {
  const {name , value} = e.target
  setFormData({...formData, [name]:value});
}

// submit if register 
const register = async (e) => {
  e.preventDefault()

  // validation if they are not exists
  if(!name || !email || !password || !confirmpassword) {
      return toast.error("All fields are required")
  }
  // validation if the password and confirm is did not match
  if(password !== confirmpassword) {
    return toast.error("Password does't match")
}

// valdate if the password must greather than 6 
if(password.length < 6 ) {
  return toast.error("Password must be up to 6 characters")
}

// Please enter a valid email
if(!validateEmail){
  return toast.error("Please Enter a valid email")
}


// define and get the data for register
const userData = {
  name,
  email,
  password

}

setIsloading(true)
try {
  const data = await resgiterUser(userData);
   console.log(data);
    await dispatch(SET_LOGIN(true))
    await dispatch(SET_NAME(data.name))
    navigate("/dashboard");
    setIsloading(false);
  } catch (error) {

}

}

  return (
    <div className={`container ${styles.auth}`}>
        {isLoading && <Loader />}
    <Card>   
      <div className={styles.form}>
          <div className="--flex-center">
                  <TiUserAddOutline size={35}/>
          </div>
          <h2>Register</h2>


          {/* FORM SECTION */}
          <form onSubmit={register}>
          <input type="text" 
            placeholder='Name..'
            required
            name='name'
            value={name} onChange={handleInputChange}
          />

          <input type="email" 
            placeholder='Email..'
            required
            name='email'
            value={email} onChange={handleInputChange}
          />

          <input type="password" 
            placeholder='Password..'
            required
            name='password'
            autoComplete="on"
            value={password} onChange={handleInputChange}
          />

            <input type="password" 
            placeholder='Confirm Password..'
            required
            name='confirmpassword'
            autoComplete="on"
            value={confirmpassword} onChange={handleInputChange}
          />


          <button type='submit' className='--btn --btn-primary --btn-block'>Register</button>

          </form>

          <span className={styles.register}>
          <Link to="/">Home</Link>
            <p>&nbsp; Already an account &nbsp;   </p>
          <Link to="/login">Login</Link>

          </span>
     </div>
        </Card>
    </div>
  )
}

export default Register
