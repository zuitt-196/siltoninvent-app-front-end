import {useState} from 'react'
import styles from "./auth.module.scss"
import {MdPassword} from "react-icons/md"
import Card from '../../components/card/Card'

// useParams method in url 
import { Link, useParams } from 'react-router-dom'
import { toast } from 'react-toastify'
import { resetPassword } from '../../services/authServices'



const initialState = {
  password: "",
  confirmPassword:""
}

  

const Reset = () => {
  // define and initial the  default data in register componenst
  const [formData, setFormData] = useState(initialState);
  
  // defracture the formData 
  const { password, confirmPassword} = formData;

  // define resetToken used the params the way of desfracturing
  // resetoken is made from url reset/:resetTokern page
  const { resetToken } = useParams()

// define  functiion that changes in input elemenent
const handleInputChange = (e) => {
  const {name , value} = e.target
  setFormData({...formData, [name]:value});

}


const reset = async (e) => {
   e.preventDefault();  
      // validation if the password and confirm is did not match
  if(password !== confirmPassword) {
    return toast.error("Password does't match")
}
  // valdate if the password must greather than 6 
  if(password.length < 6 ) {
  return toast.error("Password must be up to 6 characters ")
}
  console.log(resetToken)
// define and get the data or the reset password or new passowrd
const userData = {
  password: password,
  confirmPassword: confirmPassword
}


// define the reset password data 
try {
  const newPasswordData = await  resetPassword(userData, resetToken)
    toast.success(newPasswordData.message)
} catch (error) {
     console.log(error.message)
}





}



  return (
    <div className={`container ${styles.auth}`}>
        
    <Card>   
      <div className={styles.form}>
          <div className="--flex-center">
                  <MdPassword size={35}/>
          </div>
          <h2>Reset Password</h2>


          {/* FORM SECTION */}
          <form onSubmit={reset}>

                <input type="password" 
                  placeholder='New Password'
                  required
                  name='password'
                  value={password}
                  onChange={handleInputChange}
                  autoComplete ="on"
                />
  

                <input type="password" 
                  placeholder='Confirm new Password'
                  required
                  name='confirmPassword'
                  value={confirmPassword}
                  onChange={handleInputChange}
                  autoComplete ="on"
                />
                 

                <button type='submit' className='--btn --btn-primary --btn-block'>Reset Password</button>
                <div className={styles.links}>
                          <p>
                            <Link to="/">-Home</Link>
                          </p>

                          <p>
                            <Link to="/login">-Login</Link>
                        </p> 
                          
                  </div>
                </form>
     </div>
        </Card>
    </div>
  )
}

export default Reset
