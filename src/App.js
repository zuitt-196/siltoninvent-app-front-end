import {BrowserRouter,Routes, Route} from "react-router-dom"
import { useEffect } from "react";
// IMPORT PAGES FILE
import Home from "./pages/Home/Home";
import Login from "./pages/auth/LogIn";
import Rigester from "./pages/auth/Register";
import ForgotPassword from "./pages/auth/ForgotPassword";
import ResetPassword  from "./pages/auth/Reset";
import EditProduct from "./pages/editProduct/EditProduct";
import Addproduct from "./pages/addProduct/Addproduct ";


// IMPORT AXIOS HTTP RESQUEST 
import axios from "axios";

// IMPPORT NOTIFICATION STYLE TOASTIFY all components has acces it 
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


// IMPORT THE LAYOUT COMPONENTS
import Sidebar from "./components/sidebar/Sidebar";
import Layout from "./components/layout/Layout";
import Dashboard from "./pages/dasboard/Dashboard";
import { useDispatch } from "react-redux";
import { getLoginStatus } from "./services/authServices";
import {SET_LOGIN} from "./redux/features/auth/authSlice"
import ProductDetail from "./components/product/productDetails/ProductDetail"
import Profile from "./pages/profile/Profile";
import EditProfile from "./pages/profile/EditProfile";
import Contact from "./pages/contact/Contact";

// the purpose of it is to define if the user request the server , since the App file is  handle all components they are already acces the credential\
// it is handle all in terms requesting data
axios.defaults.withCredentials = true;

function App() {
   const  dispatch = useDispatch()

   useEffect(() => {
      const loginStatus = async () => {
        const status = await  getLoginStatus()
        dispatch(SET_LOGIN(status))
      }
      loginStatus()
   }, [dispatch])
  
  return (

  <BrowserRouter>
  <ToastContainer />
    <Routes>
       <Route path="/" element={<Home/>}/>
      <Route path="/login" element={<Login/>}/>
       <Route path="/register" element={<Rigester/>}/>
       <Route path="/forgotpassword" element={<ForgotPassword/>}/>
       <Route path="/resetpassword/:resetToken" element={<ResetPassword/>}/>
       
       

       {/* LAYOUT SECTION  */}
       <Route path="/dashboard" element={
       <Sidebar>
          <Layout>
            <Dashboard/>
          </Layout>
       </Sidebar>
      }/>
        
    <Route path="/add-product" element={
       <Sidebar>
          <Layout>
            <Addproduct/> 
          </Layout>
       </Sidebar>
      }/>

  <Route path="/product-detail/:id" element={
       <Sidebar>
          <Layout>
            <ProductDetail/>
          </Layout>
       </Sidebar>
      }/> 

    <Route path="/product-update/:id" element={
       <Sidebar>
          <Layout>
            <EditProduct/>
          </Layout>
       </Sidebar>
      }/>



  <Route path="/profile" element={
       <Sidebar>
          <Layout>
            <Profile/>
          </Layout>
       </Sidebar>
      }/>

<Route path="/edit-profile" element={
       <Sidebar>
          <Layout>
            <EditProfile/>
          </Layout>
       </Sidebar>
      }/>

    <Route path="/contact-us" element={
       <Sidebar>
          <Layout>
            <Contact/> 
          </Layout>
       </Sidebar>
      }/>



        <Route path="*" element={<h2 className="notFoudPage">Page is Not Found</h2>} />
    </Routes>

    
  </BrowserRouter>

  )
}

export default App;

