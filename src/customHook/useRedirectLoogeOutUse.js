
import React,{useEffect}from 'react'
import { useDispatch } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import {SET_LOGIN} from "../redux/features/auth/authSlice"

import {getLoginStatus } from "../services/authServices"
import {toast} from "react-toastify"

const useRedirectLoogeOutUse = (path) => {


    const navigate = useNavigate()
     const dispatch = useDispatch()


    useEffect(() => {
        // alert("Sdsad")
            const redirectLoggedOutUser = async () => {
                const isLoggedIn = await getLoginStatus()
                //  console.log(isLoggedIn)
                 dispatch(SET_LOGIN(isLoggedIn))

                 // validation if the use is login or logout
                 if (!isLoggedIn) {
                   const info = toast.info("Session expired, please login to continue") 
                    navigate(path)
                    return info
                    
                 }
            }
            redirectLoggedOutUser()

    }, [navigate,dispatch,path])
}

export default useRedirectLoogeOutUse
